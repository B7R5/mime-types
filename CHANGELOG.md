# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.8.7] - 2024-01-30

### Added

- Added support for non-standard XLS files with "Book" stream, contributed by @B7R5.

## [2.8.6] - 2023-11-09

### Added

- `.prql` file extension has been imported from source data.

## [2.8.5] - 2023-10-04

### Added

- WASM file signature.

### Changed

- Improved detection of vCard file signature.

## [2.8.4] - 2023-08-10

### Added

- `.ggs` file extension has been imported from source data.

## [2.8.3] - 2023-02-22

### Added

- `.cjs` file extension has been imported from source data.

## [2.8.2] - 2023-02-20

### Added

- `.jfif` file extension has been imported from source data.

## [2.8.1] - 2023-01-14

### Changed

- Improved detection of DOTX, XLTX and POTX file signatures.

## [2.8.0] - 2022-12-18

### Changed

- Library is built for .NET Standard 2.0.

## [2.7.2] - 2022-11-04

### Added

- TTF file signature.

## [2.7.1] - 2022-10-29

### Added

- VCARD/VCF and AMR file signatures.

### Changed

- Improved MP3 and OGG file signature detection.

## [2.7.0] - 2022-06-23

### Added

- AVIF, HEIC file signatures.

## [2.6.0] - 2022-05-21

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

### Fixed

- Fixed file signature detection of ".docx" files with a non-standard document XML.

## [2.5.0] - 2021-11-21

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.5.2, 4.6.1 and 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

## [2.4.3] - 2021-10-09

### Added

- OGG, PSD, RAR file signatures.

### Changed

- Reduced memory allocations for file signature detection.

## [2.4.2] - 2021-10-09

### Added

- 7Z file signature.

### Fixed

- Handle file signatures of empty and spanned ZIP files (issue #3).

## [2.4.1] - 2021-09-04

### Added

- `.vtt` file extension has been imported from source data.

## [2.4.0] - 2021-08-12

### Added

- Added file signature for `.webm` files. File name is also needed in order
  to be able to distinguish it from `.mkv` files (they share the same signature).

## [2.3.6] - 2021-07-05

### Added

- `.webmanifest` file extension has been imported from source data.

## [2.3.5] - 2021-06-06

### Added

- MP3 file signature.
- `.avif` file extension has been imported from source data.

## [2.3.4] - 2021-05-08

### Added

- [Added README to NuGet package](https://devblogs.microsoft.com/nuget/add-a-readme-to-your-nuget-package/).

## [2.3.3] - 2021-02-15

### Added

- `.opus` file extension has been imported from source data.

## [2.3.2] - 2020-11-08

### Added

- `application/x-zip-compressed` has been imported from source data.

### Changed

- `application/x-zip-compressed` is the default MIME type for ZIP files.

### Fixed

- MP4 file signature, when matched, will also return `audio/mp4` and `video/mp4` MIME types.

## [2.3.1] - 2020-10-17

### Added

- Added support for .NET Framework 4.5.2.

## [2.3.0] - 2020-10-03

### Added

- New methods to retrive MIME type(s) from file signature and file name.

## [2.2.0] - 2020-09-13

### Added

- New file signatures (DCM).
- OpenDocument MIME types are read from `mimetype` internal file.
- Office Open XML files are inspected to decode MIME type.

### Changed

- Maps are now lazy objects. This should improve startup time of projects using this library.

## [2.1.2] - 2020-09-06

### Added

- New file signatures (EPS, CR2, CRW, 3GP, FLV, MKV, MP4, RTF).
- OLECF file MIME type deduction using `OpenMcdf` package (DOC, PPT, XLS, VSD, MSG).
- New method to retrieve all MIME types deduced from file signature.

## [2.1.1] - 2020-08-31

### Added

- New file signatures (BMP and GIF).

## [2.1.0] - 2020-08-26

### Changed

- File signatures are evaluated using custom code.

### Removed

- Dropped dependency on `FileSignatures` package.

## [2.0.1] - 2020-08-25

### Fixed

- Restored stream position after text file check has been performed.

## [2.0.0] - 2020-08-25

### Added

- Added a dependency on `FileSignatures` package.
- Added methods to determine MIME type of a file stream.

### Changed

- `filePath` parameters have been renamed to `fileName`, to clarify
  that file is not actually read (and if that is what the user wants,
  new stream-based methods should be used).

## [1.4.1] - 2020-06-17

### Changed

- Embedded package icon and license.

## [1.4.0] - 2020-05-09

### Added

- Defined new methods to retrieve the encoding of a given MIME type.

## [1.3.2] - 2020-04-26

### Changed

- Imported new MIME types from main data source.

## [1.3.1] - 2020-04-25

### Added

- Added new mappings for RAW images.

## [1.3.0] - 2020-03-29

### Changed

- MIME type and extension constants are now `UPPER_CASE` (e.g. `MimeTypeMap.IMAGE.PNG`).

## [1.2.0] - 2020-03-28

### Added

- Added two methods to retrieve all extensions linked to a specific MIME type.

### Changed

- Library now targets .NET Standard 2.0, .NET Framework 4.6.1 and 4.7.2.
- MIME types are now fetched from [mime-types-data project](https://github.com/mime-types/mime-types-data).

## [1.1.0] - 2020-03-22

### Added

- All MIME types can be used as constants (e.g. `MimeTypeMap.Image.Png`).
- All extensions can be used as constants (e.g. `MimeTypeMap.Extensions.Png`).

### Changed

- Library is now 99% compatible with `MediaTypeMap.Core` package (see README for details).

## [1.0.1] - 2020-03-15

### Added

- Added `GetExtensionWithoutDot` utility method.

### Changed

- Renamed `fileName` parameter into `filePath`.

### Fixed

- Default `bin` extension did not contain the dot character.

## [1.0.0] - 2020-03-14

### Added

- Initial release.

[2.8.7]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.6...2.8.7
[2.8.6]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.5...2.8.6
[2.8.5]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.4...2.8.5
[2.8.4]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.3...2.8.4
[2.8.3]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.2...2.8.3
[2.8.2]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.1...2.8.2
[2.8.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.8.0...2.8.1
[2.8.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.7.2...2.8.0
[2.7.2]: https://gitlab.com/pommalabs/mime-types/-/compare/2.7.1...2.7.2
[2.7.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.7.0...2.7.1
[2.7.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.6.0...2.7.0
[2.6.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.5.0...2.6.0
[2.5.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.4.3...2.5.0
[2.4.3]: https://gitlab.com/pommalabs/mime-types/-/compare/2.4.2...2.4.3
[2.4.2]: https://gitlab.com/pommalabs/mime-types/-/compare/2.4.1...2.4.2
[2.4.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.4.0...2.4.1
[2.4.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.6...2.4.0
[2.3.6]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.5...2.3.6
[2.3.5]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.4...2.3.5
[2.3.4]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.3...2.3.4
[2.3.3]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.2...2.3.3
[2.3.2]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.1...2.3.2
[2.3.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.3.0...2.3.1
[2.3.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.2.0...2.3.0
[2.2.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.1.2...2.2.0
[2.1.2]: https://gitlab.com/pommalabs/mime-types/-/compare/2.1.1...2.1.2
[2.1.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.1.0...2.1.1
[2.1.0]: https://gitlab.com/pommalabs/mime-types/-/compare/2.0.1...2.1.0
[2.0.1]: https://gitlab.com/pommalabs/mime-types/-/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.com/pommalabs/mime-types/-/compare/1.4.1...2.0.0
[1.4.1]: https://gitlab.com/pommalabs/mime-types/-/compare/1.4.0...1.4.1
[1.4.0]: https://gitlab.com/pommalabs/mime-types/-/compare/1.3.2...1.4.0
[1.3.2]: https://gitlab.com/pommalabs/mime-types/-/compare/1.3.1...1.3.2
[1.3.1]: https://gitlab.com/pommalabs/mime-types/-/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.com/pommalabs/mime-types/-/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/pommalabs/mime-types/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/pommalabs/mime-types/-/compare/1.0.1...1.1.0
[1.0.1]: https://gitlab.com/pommalabs/mime-types/-/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/pommalabs/mime-types/-/tags/1.0.0
