﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using DotLiquid;
using Newtonsoft.Json;

namespace PommaLabs.MimeTypes.Generator;

internal readonly struct MimeData : ILiquidizable
{
    public MimeData(string mimeType, string encoding)
        : this(mimeType, encoding, new List<string>(1))
    {
    }

    [JsonConstructor]
    public MimeData(string mimeType, string encoding, List<string> extensions)
    {
        MimeType = mimeType;
        Encoding = encoding;
        Extensions = extensions;
    }

    [JsonProperty("encoding")]
    public string Encoding { get; }

    [JsonProperty("extensions")]
    public List<string> Extensions { get; }

    [JsonProperty("content-type")]
    public string MimeType { get; }

    public object ToLiquid()
    {
        return new
        {
            mime_type = MimeType,
            encoding = "MimeEncoding." + Encoding switch
            {
                "7bit" => "_7bit",
                "8bit" => "_8bit",
                "base64" => "Base64",
                "quoted-printable" => "QuotedPrintable",
                _ => throw new InvalidOperationException("Unrecognized encoding")
            },
            extensions = Extensions
        };
    }
}
