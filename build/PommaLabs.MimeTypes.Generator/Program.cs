﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DotLiquid;
using Newtonsoft.Json;
using StringMultiMap = System.Collections.Generic.SortedList<string, System.Collections.Generic.SortedSet<string>>;
using StructMultiMap = System.Collections.Generic.SortedList<string, PommaLabs.MimeTypes.Generator.MimeData>;
using StructMultiMap2 = System.Collections.Generic.SortedList<string, System.Collections.Generic.SortedList<string, PommaLabs.MimeTypes.Generator.MimeData>>;

namespace PommaLabs.MimeTypes.Generator;

/// <summary>
///   Source code generator.
/// </summary>
public static partial class Program
{
    private static readonly Uri s_sourceDataUri = new("https://raw.githubusercontent.com/mime-types/mime-types-data/master/data/mime-types.json");
    private static readonly char[] s_mimeTypeSeparator = ['/'];

    /// <summary>
    ///   Entry point.
    /// </summary>
    /// <param name="args">Accepts "src" or "test".</param>
    /// <returns>0 if OK, 1 if KO.</returns>
    public static async Task<int> Main(string[] args)
    {
        if (args == null || args.Length != 1)
        {
            // First argument should be "src" in order to generate source code of MimeTypeMap or
            // it should be "test" in order to generate unit tests.
            return 1;
        }

        Template.RegisterFilter(typeof(CustomFilters));

        var (extensions, mimeTypes) = await GetExtensionsAndMimeTypesAsync();

        switch (args[0])
        {
            case "src":
                Console.WriteLine(GenerateMimeTypeMap(extensions, mimeTypes));
                return 0;

            case "tests":
                Console.WriteLine(GenerateMimeTypeMapTests(extensions, mimeTypes));
                return 0;

            default:
                return 1;
        }
    }

    private static string GenerateMimeTypeMap(StringMultiMap extensions, StructMultiMap2 mimeTypes)
    {
        var templateContents = File.ReadAllText("MimeTypeMap.liquid");
        var template = Template.Parse(templateContents);

        return template.Render(Hash.FromAnonymousObject(new
        {
            source_url = s_sourceDataUri.AbsoluteUri,
            extensions,
            mime_types = mimeTypes,
        }));
    }

    private static string GenerateMimeTypeMapTests(StringMultiMap extensions, StructMultiMap2 mimeTypes)
    {
        var templateContents = File.ReadAllText("MimeTypeMapTests.liquid");
        var template = Template.Parse(templateContents);

        return template.Render(Hash.FromAnonymousObject(new
        {
            extensions,
            mime_types = mimeTypes,
        }));
    }

    private static async Task<(StringMultiMap extensions, StructMultiMap2 mimeTypes)> GetExtensionsAndMimeTypesAsync()
    {
        using var httpClient = new HttpClient();
        var response = await httpClient.GetStringAsync(s_sourceDataUri);

        var extensions = new StringMultiMap(StringComparer.OrdinalIgnoreCase);
        var mimeTypes = new StructMultiMap2(StringComparer.OrdinalIgnoreCase);

        using var reader = new StringReader(response);
        var sourceData = JsonConvert.DeserializeObject<MimeData[]>(reader.ReadToEnd());

        foreach (var data in sourceData.OrderBy(x => x.MimeType))
        {
            ProcessSingleMimeData(data, extensions, mimeTypes);
        }

        // Following overrides ensures best compatibility with MediaTypeMap.Core results.
        // Complete compatibility cannot be achieved because some results of that library were
        // simply wrong and they are documented in this project README.
        OverrideExistingMimeTypes(mimeTypes);

        return (extensions, mimeTypes);
    }

    private static void OverrideExistingMimeTypes(StructMultiMap2 mimeTypes)
    {
        SwapExtensions(mimeTypes["application"]["pkcs7-mime"], "p7m", "p7c");
        SwapExtensions(mimeTypes["application"]["vnd.ms-works"], "wcm", "wks");
        SwapExtensions(mimeTypes["application"]["x-director"], "dcr", "dir");
        SwapExtensions(mimeTypes["application"]["x-latex"], "ltx", "latex");
        SwapExtensions(mimeTypes["application"]["x-msaccess"], "mda", "mdb");
        SwapExtensions(mimeTypes["application"]["x-msdownload"], "exe", "dll");
        SwapExtensions(mimeTypes["application"]["x-msmetafile"], "emf", "wmf");
        SwapExtensions(mimeTypes["application"]["x-texinfo"], "texinfo", "texi");
        SwapExtensions(mimeTypes["application"]["x-troff"], "t", "roff");
        SwapExtensions(mimeTypes["application"]["xhtml+xml"], "xht", "xhtml");
        SwapExtensions(mimeTypes["audio"]["basic"], "au", "snd");
        SwapExtensions(mimeTypes["audio"]["mpeg"], "mpga", "mp3");
        SwapExtensions(mimeTypes["image"]["jpeg"], "jpeg", "jpg");
        SwapExtensions(mimeTypes["video"]["mpeg"], "mp2", "mpg");
        SwapExtensions(mimeTypes["video"]["ogg"], "ogg", "ogv");
        SwapExtensions(mimeTypes["video"]["quicktime"], "qt", "mov");
        SwapExtensions(mimeTypes["video"]["x-matroska"], "mk3d", "mkv");
    }

    private static void ProcessSingleExtension(MimeData data, StringMultiMap extensions, StructMultiMap2 mimeTypes, string extension, string mimeType)
    {
        var splitMimeType = mimeType.Split(s_mimeTypeSeparator, StringSplitOptions.RemoveEmptyEntries);
        var type = splitMimeType[0];
        var subtype = splitMimeType[1];

        if (!extensions.TryGetValue(extension, out var extensionMimeTypes))
        {
            extensions[extension] = extensionMimeTypes = new SortedSet<string>(StringComparer.OrdinalIgnoreCase);
        }
        if (!extensionMimeTypes.Contains(mimeType, StringComparer.OrdinalIgnoreCase))
        {
            extensionMimeTypes.Add(mimeType);
        }

        if (!mimeTypes.TryGetValue(type, out var subtypes))
        {
            mimeTypes[type] = subtypes = new StructMultiMap(StringComparer.OrdinalIgnoreCase);
        }
        if (!subtypes.TryGetValue(subtype, out var subtypeData))
        {
            subtypes[subtype] = subtypeData = new MimeData(mimeType, data.Encoding);
        }
        if (!subtypeData.Extensions.Contains(extension))
        {
            subtypeData.Extensions.Add(extension);
        }
    }

    private static void ProcessSingleMimeData(MimeData data, StringMultiMap extensions, StructMultiMap2 mimeTypes)
    {
        if (data.Extensions == null || data.Extensions.Count == 0)
        {
            return;
        }

        var mimeType = data.MimeType.ToLowerInvariant();
        var extensionList = data.Extensions.Select(x => x.ToLowerInvariant());

        foreach (var extension in extensionList)
        {
            ProcessSingleExtension(data, extensions, mimeTypes, extension, mimeType);
        }
    }

    private static void SwapExtensions(MimeData data, string ext1, string ext2)
    {
        var list = data.Extensions;

        var i1 = list.IndexOf(ext1);
        if (i1 != 0)
        {
            throw new InvalidOperationException($"{ext1} is not the first item of list [{string.Join(", ", list)}]");
        }

        var i2 = list.IndexOf(ext2);
        if (i2 < 0)
        {
            throw new InvalidOperationException($"{ext2} was not found in list [{string.Join(", ", list)}]");
        }

        list[i1] = ext2;
        list[i2] = ext1;
    }
}
