﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http.Headers;
using OpenMcdf;

namespace PommaLabs.MimeTypes;

public static partial class MimeTypeMap
{
    /// <summary>
    ///   <para>
    ///     Source data:
    ///     - https://www.garykessler.net/library/file_sigs.html
    ///     - https://en.wikipedia.org/wiki/List_of_file_signatures
    ///     - https://asecuritysite.com/forensics/magic
    ///   </para>
    ///   <para>Comments are copied from source data, in order to have a quick reference.</para>
    /// </summary>
    private static readonly Lazy<List<FileSignature>> s_fileSignatures = new(() => new List<FileSignature>
    {
        new FileSignature
        {
            // 3rd Generation Partnership Project 3GPP multimedia files.
            Offset = 4,
            Signature = new ByteRange[] { 0x66, 0x74, 0x79, 0x70, 0x33, 0x67, 0x70 },
            MimeTypes = _ => s_extensionMap.Value[Extensions._3GP].Union(s_extensionMap.Value[Extensions._3G2]).ToList()
        },
        new FileSignature
        {
            // 7-Zip File Format.
            Signature = new ByteRange[] { 0x37, 0x7A, 0xBC, 0xAF, 0x27, 0x1C },
            MimeTypes = _ => s_extensionMap.Value[Extensions._7Z]
        },
        new FileSignature
        {
            // Adaptive Multi-Rate ACELP (Algebraic Code Excited Linear Prediction)
            // Codec, commonly audio format with GSM cell phones. (See RFC 4867.)
            Signature = new ByteRange[] { 0x23, 0x21, 0x41, 0x4D, 0x52 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.AMR]
        },
        new FileSignature
        {
            // Resource Interchange File Format - Windows Audio Video Interleave file.
            Signature = new ByteRange[] { 0x52, 0x49, 0x46, 0x46, ByteRange.Any, ByteRange.Any, ByteRange.Any, ByteRange.Any, 0x41, 0x56, 0x49, 0x20, 0x4C, 0x49, 0x53, 0x54 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.AVI]
        },
        new FileSignature
        {
            // Alliance for Open Media (AOMedia) Video 1 (AV1) Image File - ftypavif.
            Signature = new ByteRange[] { 0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x61, 0x76, 0x69, 0x66 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.AVIF]
        },
        new FileSignature
        {
            // Windows (or device-independent) bitmap image.
            Signature = new ByteRange[] { 0x42, 0x4D },
            MimeTypes = _ => s_extensionMap.Value[Extensions.BMP]
        },
        new FileSignature
        {
            // Canon digital camera RAW file.
            Signature = new ByteRange[] { 0x49, 0x49, 0x2A, 0x00, 0x10, 0x00, 0x00, 0x00, 0x43, 0x52 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.CR2]
        },
        new FileSignature
        {
            // Canon digital camera RAW file.
            Signature = new ByteRange[] { 0x49, 0x49, 0x1A, 0x00, 0x00, 0x00, 0x48, 0x45, 0x41, 0x50, 0x43, 0x43, 0x44, 0x52, 0x02, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.CRW]
        },
        new FileSignature
        {
            // Canon digital camera RAW file.
            Signature = new ByteRange[] { 0xC2, 0x07 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.CRW]
        },
        new FileSignature
        {
            // DICOM Medical File Format.
            Offset = 128,
            Signature = new ByteRange[] { 0x44, 0x49, 0x43, 0x4D },
            MimeTypes = _ => s_extensionMap.Value[Extensions.DCM]
        },
        new FileSignature
        {
            // Adobe encapsulated PostScript file.
            Signature = new ByteRange[] { 0x25, 0x21, 0x50, 0x53 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.EPS]
        },
        new FileSignature
        {
            // Flash video file.
            Signature = new ByteRange[] { 0x46, 0x4C, 0x56, 0x01 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.FLV]
        },
        new FileSignature
        {
            // Graphics interchange format file.
            Signature = new ByteRange[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.GIF]
        },
        new FileSignature
        {
            // Graphics interchange format file.
            Signature = new ByteRange[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.GIF]
        },
        new FileSignature
        {
            // High Efficiency Image Container (HEIC) - ftypheic.
            Signature = new ByteRange[] { 0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x68, 0x65, 0x69, 0x63 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.HEIC]
        },
        new FileSignature
        {
            // High Efficiency Image Container (HEIC) - ftypmif1.
            Signature = new ByteRange[] { 0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x6D, 0x69, 0x66, 0x31 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.HEIC]
        },
        new FileSignature
        {
            // Generic JPEG image file.
            Signature = new ByteRange[] { 0xFF, 0xD8 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.JPEG]
        },
        new FileSignature
        {
            // Matroska stream file.
            Signature = new ByteRange[] { 0x1A, 0x45, 0xDF, 0xA3 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.MKV].Union(s_extensionMap.Value[Extensions.WEBM]).ToList()
        },
        new FileSignature
        {
            // MP3 file with an ID3v2 container.
            Signature = new ByteRange[] { 0x49, 0x44, 0x33 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.MP3]
        },
        new FileSignature
        {
            // MPEG-1 Layer 3 file without an ID3 tag or with an ID3v1 tag (which is appended at
            // the end of the file).
            Signature = new ByteRange[] { 0xFF, new ByteRange(0xE0, 0xEF) },
            MimeTypes = _ => s_extensionMap.Value[Extensions.MP3]
        },
        new FileSignature
        {
            // MPEG-1 Layer 3 file without an ID3 tag or with an ID3v1 tag (which is appended at
            // the end of the file).
            Signature = new ByteRange[] { 0xFF, new ByteRange(0xF0, 0xFF) },
            MimeTypes = _ => s_extensionMap.Value[Extensions.MP3]
        },
        new FileSignature
        {
            // ISO Base Media file (MPEG-4) v1.
            Offset = 4,
            Signature = new ByteRange[] { 0x66, 0x74, 0x79, 0x70, 0x69, 0x73, 0x6F, 0x6D },
            MimeTypes = _ => s_extensionMap.Value[Extensions.MP4]
        },
        new FileSignature
        {
            // Ogg Vorbis Codec compressed Multimedia file.
            Signature = new ByteRange[] { 0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.OGG]
        },
        new FileSignature
        {
            // Adobe Portable Document Format, Forms Document Format, and Illustrator graphics files.
            Signature = new ByteRange[] { 0x25, 0x50, 0x44, 0x46 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.PDF]
        },
        new FileSignature
        {
            // Portable Network Graphics file.
            Signature = new ByteRange[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A },
            MimeTypes = _ => s_extensionMap.Value[Extensions.PNG]
        },
        new FileSignature
        {
            // Photoshop image file.
            Signature = new ByteRange[] { 0x38, 0x42, 0x50, 0x53 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.PSD]
        },
        new FileSignature
        {
            // Rich text format word processing file.
            Signature = new ByteRange[] { 0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.RAR]
        },
        new FileSignature
        {
            // Rich text format word processing file.
            Signature = new ByteRange[] { 0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x01, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.RAR]
        },
        new FileSignature
        {
            // Rich text format word processing file.
            Signature = new ByteRange[] { 0x7B, 0x5C, 0x72, 0x74, 0x66 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.RTF]
        },
        new FileSignature
        {
            // Tagged Image File Format file.
            Signature = new ByteRange[] { 0x49, 0x20, 0x49 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF]
        },
        new FileSignature
        {
            // Tagged Image File Format file (little endian, i.e., LSB first in the byte; Intel).
            Signature = new ByteRange[] { 0x49, 0x49, 0x2A, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF]
        },
        new FileSignature
        {
            // Tagged Image File Format file (big endian, i.e., LSB last in the byte; Motorola).
            Signature = new ByteRange[] { 0x4D, 0x4D, 0x00, 0x2A },
            MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF]
        },
        new FileSignature
        {
            // BigTIFF files; Tagged Image File Format files >4 GB.
            Signature = new ByteRange[] { 0x4D, 0x4D, 0x00, 0x2B },
            MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF]
        },
        new FileSignature
        {
            // TrueType font file.
            Signature = new ByteRange[] { 0x00, 0x01, 0x00, 0x00, 0x00 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.TTF]
        },
        new FileSignature
        {
            // vCard file with CR/LF line endings.
            Signature = new ByteRange[] { 0x42, 0x45, 0x47, 0x49, 0x4E, 0x3A, 0x56, 0x43, 0x41, 0x52, 0x44, 0x0D, 0x0A },
            MimeTypes = _ => s_extensionMap.Value[Extensions.VCARD].Union(s_extensionMap.Value[Extensions.VCF]).ToList()
        },
        new FileSignature
        {
            // vCard file with LF line ending.
            Signature = new ByteRange[] { 0x42, 0x45, 0x47, 0x49, 0x4E, 0x3A, 0x56, 0x43, 0x41, 0x52, 0x44, 0x0A },
            MimeTypes = _ => s_extensionMap.Value[Extensions.VCARD].Union(s_extensionMap.Value[Extensions.VCF]).ToList()
        },
        new FileSignature
        {
            // WebAssembly binary format.
            Signature = new ByteRange[] { 0x00, 0x61, 0x73, 0x6D },
            MimeTypes = _ => s_extensionMap.Value[Extensions.WASM]
        },
        new FileSignature
        {
            // Google WebP image file, where "xx xx xx xx" is the file size.
            Signature = new ByteRange[] { 0x52, 0x49, 0x46, 0x46, ByteRange.Any, ByteRange.Any, ByteRange.Any, ByteRange.Any, 0x57, 0x45, 0x42, 0x50 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.WEBP]
        },
        new FileSignature
        {
            // PKZIP archive file.
            Signature = new ByteRange[] { 0x50, 0x4B, 0x03, 0x04 },
            MimeTypes = InspectZipFileSignature
        },
        new FileSignature
        {
            // PKZIP archive file (empty). Since it is empty, it is not necessary to inspect it thoroughly.
            Signature = new ByteRange[] { 0x50, 0x4B, 0x05, 0x06 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.ZIP]
        },
        new FileSignature
        {
            // PKZIP archive file (spanned). Since it is a part of a multi-file ZIP, it is not
            // possible to inspect it thoroughly.
            Signature = new ByteRange[] { 0x50, 0x4B, 0x07, 0x08 },
            MimeTypes = _ => s_extensionMap.Value[Extensions.ZIP]
        },
        new FileSignature
        {
            // An Object Linking and Embedding (OLE) Compound File (CF) (i.e., OLECF) file
            // format, known as Compound Binary File format by Microsoft, used by Microsoft
            // Office 97-2003 applications (Word, PowerPoint, Excel, Wizard).
            Signature = new ByteRange[] { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 },
            MimeTypes = InspectOlecfFileSignature
        },
    }.OrderByDescending(x => x.Signature.Length /* Longest signatures should be evaluated first */).ToList());

    /// <summary>
    ///   <para>Used to identify OLECF files. Each stream name is probed again file root storage.</para>
    ///   <para>Source data: https://github.com/neilharvey/FileSignatures</para>
    /// </summary>
    private static readonly Lazy<Dictionary<string, IList<string>>> s_olecfStreamProbes = new(() => new Dictionary<string, IList<string>>
    {
        ["__properties_version1.0"] = s_extensionMap.Value[Extensions.MSG],
        ["Book"] = s_extensionMap.Value[Extensions.XLS],
        ["PowerPoint Document"] = s_extensionMap.Value[Extensions.PPT],
        ["VisioDocument"] = s_extensionMap.Value[Extensions.VSD],
        ["WordDocument"] = s_extensionMap.Value[Extensions.DOC],
        ["Workbook"] = s_extensionMap.Value[Extensions.XLS],
    });

    /// <summary>
    ///   How long should be the buffer used to probe file signatures.
    /// </summary>
    private static readonly Lazy<int> s_probeBufferLength = new(() => s_fileSignatures.Value.Max(s => s.Signature.Length + s.Offset));

    private static IList<string> InspectOlecfFileSignature(Stream fileStream)
    {
        // Restore stream position. Stream supports seeking, otherwise file signature operation,
        // which happens before this method, would have failed.
        fileStream.Position = 0L;

        try
        {
            using var compoundFile = new CompoundFile(fileStream, CFSUpdateMode.ReadOnly, CFSConfiguration.LeaveOpen);
            var rootStorage = compoundFile.RootStorage;

            return s_olecfStreamProbes!.Value
                .Where(sp => rootStorage.TryGetStream(sp.Key, out var _))
                .Select(sp => sp.Value)
                .SingleOrDefault() ?? Array.Empty<string>();
        }
        catch
        {
            // Nothing to do.
        }

        // No match has been found.
        return Array.Empty<string>();
    }

    private static IList<string> InspectZipFileSignature(Stream fileStream)
    {
        try
        {
            using var zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read, leaveOpen: true);

            foreach (var entry in zipArchive.Entries)
            {
                switch (entry.FullName?.ToLowerInvariant())
                {
                    // OpenDocument (ODT, ODS, ODP, ...).
                    case "mimetype":
                        using (var entryStream = entry.Open())
                        using (var streamReader = new StreamReader(entryStream))
                        {
                            var rawMimeType = streamReader.ReadToEnd();
                            if (MediaTypeHeaderValue.TryParse(rawMimeType, out var parsedMimeType) && parsedMimeType.MediaType != null)
                            {
                                return new[] { parsedMimeType.MediaType };
                            }
                            throw new InvalidDataException($"OpenDocument MIME type \"{rawMimeType}\" could not be parsed");
                        }

                    // Office Open XML.
                    case "xl/workbook.xml":
                        return s_extensionMap.Value[Extensions.XLSX]
                            .Union(s_extensionMap.Value[Extensions.XLTX])
                            .ToList();

                    case "ppt/presentation.xml":
                        return s_extensionMap.Value[Extensions.PPTX]
                            .Union(s_extensionMap.Value[Extensions.POTX])
                            .ToList();

                    case "word/document.xml":
                    case "word/document2.xml":
                        return s_extensionMap.Value[Extensions.DOCX]
                            .Union(s_extensionMap.Value[Extensions.DOTX])
                            .ToList();

                    case "fixeddocseq.fdseq":
                        return s_extensionMap.Value[Extensions.XPS];
                }
            }
        }
        catch (InvalidDataException)
        {
            // Corrupted ZIP file.
            return Array.Empty<string>();
        }

        // It seems to be a simple ZIP file.
        return s_extensionMap.Value[Extensions.ZIP];
    }

    private struct FileSignature
    {
        public Func<Stream, IList<string>> MimeTypes { get; set; }

        public int Offset { get; set; }

        public ByteRange[] Signature { get; set; }
    }

    private struct ByteRange
    {
        public static ByteRange Any { get; } = new ByteRange(byte.MinValue, byte.MaxValue);

        public byte MinValue { get; }

        public byte MaxValue { get; }

        public ByteRange(byte value)
        {
            MinValue = value;
            MaxValue = value;
        }

        public ByteRange(byte minValue, byte maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public bool Contains(byte value) => MinValue <= value && value <= MaxValue;

        public override string ToString() => $"{MinValue:X2}, {MaxValue:X2}";

        public static implicit operator ByteRange(int value) => new((byte)value);
    }
}
