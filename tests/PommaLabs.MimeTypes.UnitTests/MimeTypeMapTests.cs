﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.MimeTypes.UnitTests;

[TestFixture, Parallelizable]
public sealed partial class MimeTypeMapTests
{
    private const string BreadExtension = ".bread";
    private const string BreadMimeType = "image/bread";
    private const MimeEncoding DefaultEncoding = MimeEncoding.Base64;
    private const string DefaultExtension = ".bin";
    private const string DefaultMimeType = "application/octet-stream";
    private const string SampleFilesDirectory = "SampleFiles";
    private const string Tomato2Extension = ".tomato2";
    private const string Tomato3Extension = ".tomato3";
    private const string TomatoExtension = ".tomato";
    private const string TomatoMimeType = "text/tomato";

    [Test]
    public void AddOrUpdate_AllCases_ShouldBeCorrectlyHandled()
    {
        // Following test is not formally correct because there is no way to reset the map,
        // allowing the test to be split into multiple cases.

        MimeTypeMap.AddOrUpdate(TomatoMimeType, TomatoExtension);
        MimeTypeMap.GetEncoding(TomatoMimeType).ShouldBe(DefaultEncoding);
        MimeTypeMap.GetExtension(TomatoMimeType).ShouldBe(TomatoExtension);
        MimeTypeMap.GetMimeType(TomatoExtension).ShouldBe(TomatoMimeType);

        MimeTypeMap.AddOrUpdate(TomatoMimeType, MimeEncoding.QuotedPrintable, TomatoExtension);
        MimeTypeMap.GetEncoding(TomatoMimeType).ShouldBe(MimeEncoding.QuotedPrintable);
        MimeTypeMap.GetExtension(TomatoMimeType).ShouldBe(TomatoExtension);
        MimeTypeMap.GetMimeType(TomatoExtension).ShouldBe(TomatoMimeType);

        MimeTypeMap.AddOrUpdate(TomatoMimeType, new[] { Tomato2Extension, Tomato3Extension });
        MimeTypeMap.GetEncoding(TomatoMimeType).ShouldBe(DefaultEncoding);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldNotContain(TomatoExtension);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldContain(Tomato2Extension);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldContain(Tomato3Extension);
        MimeTypeMap.GetMimeTypes(TomatoExtension).ShouldBeEmpty();
        MimeTypeMap.GetMimeTypes(Tomato2Extension).ShouldContain(TomatoMimeType);
        MimeTypeMap.GetMimeTypes(Tomato3Extension).ShouldContain(TomatoMimeType);

        MimeTypeMap.AddOrUpdate(TomatoMimeType, MimeEncoding.QuotedPrintable, new[] { Tomato2Extension, Tomato3Extension });
        MimeTypeMap.GetEncoding(TomatoMimeType).ShouldBe(MimeEncoding.QuotedPrintable);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldNotContain(TomatoExtension);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldContain(Tomato2Extension);
        MimeTypeMap.GetExtensions(TomatoMimeType).ShouldContain(Tomato3Extension);
        MimeTypeMap.GetMimeTypes(TomatoExtension).ShouldBeEmpty();
        MimeTypeMap.GetMimeTypes(Tomato2Extension).ShouldContain(TomatoMimeType);
        MimeTypeMap.GetMimeTypes(Tomato3Extension).ShouldContain(TomatoMimeType);
    }

    [Test]
    public void GetEncoding_MissingMimeType_ShouldBeMatchedToBinExtensionWhenThrowExceptionIsFalse()
    {
        MimeTypeMap.GetEncoding(BreadMimeType, false).ShouldBe(DefaultEncoding);
    }

    [Test]
    public void GetEncoding_MissingMimeType_ShouldCauseArgumentException()
    {
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetEncoding(BreadMimeType));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetEncoding(BreadMimeType, true));
    }

    [Test]
    public void GetExtension_MissingMimeType_ShouldBeMatchedToBinExtensionWhenThrowExceptionIsFalse()
    {
        MimeTypeMap.GetExtension(BreadMimeType, false).ShouldBe(DefaultExtension);
        MimeTypeMap.GetExtensions(BreadMimeType).ShouldBeEmpty();
    }

    [Test]
    public void GetExtension_MissingMimeType_ShouldCauseArgumentException()
    {
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetExtension(BreadMimeType));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetExtension(BreadMimeType, true));
    }

    [Test]
    public void GetExtensionWithoutDot_MissingMimeType_ShouldBeMatchedToBinExtensionWhenThrowExceptionIsFalse()
    {
        MimeTypeMap.GetExtensionWithoutDot(BreadMimeType, false).ShouldBe(DefaultExtension.Substring(1));
        MimeTypeMap.GetExtensionsWithoutDot(BreadMimeType).ShouldBeEmpty();
    }

    [Test]
    public void GetExtensionWithoutDot_MissingMimeType_ShouldCauseArgumentException()
    {
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetExtensionWithoutDot(BreadMimeType));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetExtensionWithoutDot(BreadMimeType, true));
    }

    [Test]
    public void GetMimeType_MissingExtension_ShouldBeMatchedToApplicationOctetStreamMimeTypeWhenThrowExceptionIsFalse()
    {
        MimeTypeMap.GetMimeType($"file{BreadExtension}", false).ShouldBe(DefaultMimeType);
    }

    [Test]
    public void GetMimeType_MissingExtensionShouldCauseArgumentException()
    {
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetMimeType($"file{BreadExtension}"));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetMimeType($"file{BreadExtension}", true));
    }

    [TestCase("900.ods")]
    [TestCase("901.fakeods")]
    [TestCase("902.exe")]
    [TestCase("903.fakeexe")]
    public void GetMimeType_StreamsWithUnknownFileSignatures_ShouldBeMatchedToApplicationOctetStreamMimeTypeWhenThrowExceptionIsFalse(string fileName)
    {
        using var fs = File.OpenRead(Path.Combine(SampleFilesDirectory, fileName));
        MimeTypeMap.GetMimeType(fs, false).ShouldBe(DefaultMimeType);
    }

    [TestCase("900.ods")]
    [TestCase("901.fakeods")]
    [TestCase("902.exe")]
    [TestCase("903.fakeexe")]
    public void GetMimeType_StreamsWithUnknownFileSignatures_ShouldCauseArgumentException(string fileName)
    {
        using var fs = File.OpenRead(Path.Combine(SampleFilesDirectory, fileName));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetMimeType(fs));
        Should.Throw<ArgumentException>(() => MimeTypeMap.GetMimeType(fs, true));
    }

    [TestCase("000.jpg", MimeTypeMap.IMAGE.JPEG)]
    [TestCase("001.fakejpg", MimeTypeMap.IMAGE.JPEG)]
    [TestCase("002.png", MimeTypeMap.IMAGE.PNG)]
    [TestCase("003.fakepng", MimeTypeMap.IMAGE.PNG)]
    [TestCase("004.docx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT)]
    [TestCase("005.fakedocx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT)]
    [TestCase("006.tiff", MimeTypeMap.IMAGE.TIFF)]
    [TestCase("007.faketiff", MimeTypeMap.IMAGE.TIFF)]
    [TestCase("008.avi", MimeTypeMap.VIDEO.X_MSVIDEO)]
    [TestCase("009.fakeavi", MimeTypeMap.VIDEO.X_MSVIDEO)]
    [TestCase("010.txt", MimeTypeMap.TEXT.PLAIN)]
    [TestCase("011.faketxt", MimeTypeMap.TEXT.PLAIN)]
    [TestCase("012.fakecss", MimeTypeMap.TEXT.PLAIN)]
    [TestCase("013.fakehtml", MimeTypeMap.TEXT.PLAIN)]
    [TestCase("014.webp", MimeTypeMap.IMAGE.WEBP)]
    [TestCase("015.fakewebp", MimeTypeMap.IMAGE.WEBP)]
    [TestCase("016.pdf", MimeTypeMap.APPLICATION.PDF)]
    [TestCase("017.gif", MimeTypeMap.IMAGE.GIF)]
    [TestCase("018.bmp", MimeTypeMap.IMAGE.BMP)]
    [TestCase("019.cr2", MimeTypeMap.IMAGE.X_CANON_CR2)]
    [TestCase("020.3gp", MimeTypeMap.VIDEO._3GPP)]
    [TestCase("021.crw", MimeTypeMap.IMAGE.X_CANON_CRW)]
    [TestCase("022.eps", MimeTypeMap.APPLICATION.POSTSCRIPT)]
    [TestCase("023.flv", MimeTypeMap.VIDEO.X_FLV)]
    [TestCase("024.mkv", MimeTypeMap.VIDEO.X_MATROSKA)]
    [TestCase("025.mp4", MimeTypeMap.APPLICATION.MP4)]
    [TestCase("026.rtf", MimeTypeMap.APPLICATION.RTF)]
    [TestCase("027.xls", MimeTypeMap.APPLICATION.EXCEL)]
    [TestCase("028.doc", MimeTypeMap.APPLICATION.MSWORD)]
    [TestCase("029.xls", MimeTypeMap.APPLICATION.EXCEL)]
    [TestCase("030.ppt", MimeTypeMap.APPLICATION.POWERPOINT)]
    [TestCase("031.vsd", MimeTypeMap.APPLICATION.VND_VISIO)]
    [TestCase("032.msg", MimeTypeMap.APPLICATION.VND_MS_OUTLOOK)]
    [TestCase("033.dcm", MimeTypeMap.APPLICATION.DICOM)]
    [TestCase("034.ods", MimeTypeMap.APPLICATION.VND_OASIS_OPENDOCUMENT_SPREADSHEET)]
    [TestCase("035.odf", MimeTypeMap.APPLICATION.VND_OASIS_OPENDOCUMENT_FORMULA)]
    [TestCase("036.odg", MimeTypeMap.APPLICATION.VND_OASIS_OPENDOCUMENT_GRAPHICS)]
    [TestCase("037.odp", MimeTypeMap.APPLICATION.VND_OASIS_OPENDOCUMENT_PRESENTATION)]
    [TestCase("038.odt", MimeTypeMap.APPLICATION.VND_OASIS_OPENDOCUMENT_TEXT)]
    [TestCase("039.pptx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION)]
    [TestCase("040.xps", MimeTypeMap.APPLICATION.VND_MS_XPSDOCUMENT)]
    [TestCase("041.xlsx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET)]
    [TestCase("042.docx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT)]
    [TestCase("043.mp3", MimeTypeMap.AUDIO.MPEG)]
    [TestCase("044.webm", MimeTypeMap.VIDEO.X_MATROSKA, MimeTypeMap.AUDIO.WEBM)]
    [TestCase("045.fakewebm", MimeTypeMap.VIDEO.X_MATROSKA)]
    [TestCase("046.cs", MimeTypeMap.TEXT.PLAIN)]
    [TestCase("047.zip", MimeTypeMap.APPLICATION.X_ZIP_COMPRESSED)]
    [TestCase("048.zip", MimeTypeMap.APPLICATION.X_ZIP_COMPRESSED)]
    [TestCase("049.zip", MimeTypeMap.APPLICATION.X_ZIP_COMPRESSED)]
    [TestCase("050.7z", MimeTypeMap.APPLICATION.X_7Z_COMPRESSED)]
    [TestCase("051.ogg", MimeTypeMap.AUDIO.OGG)]
    [TestCase("052.psd", MimeTypeMap.IMAGE.VND_ADOBE_PHOTOSHOP)]
    [TestCase("053.rar", MimeTypeMap.APPLICATION.X_RAR_COMPRESSED)]
    [TestCase("054.gif", MimeTypeMap.IMAGE.GIF)]
    [TestCase("055.docx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT)]
    [TestCase("056.avif", MimeTypeMap.IMAGE.AVIF)]
    [TestCase("057.heic", MimeTypeMap.IMAGE.HEIC)]
    [TestCase("058.heic", MimeTypeMap.IMAGE.HEIC)]
    [TestCase("059.vcf", MimeTypeMap.TEXT.VCARD, MimeTypeMap.TEXT.X_VCARD)]
    [TestCase("060.mp3", MimeTypeMap.AUDIO.MPEG)]
    [TestCase("061.amr", MimeTypeMap.AUDIO.AMR)]
    [TestCase("062.mp3", MimeTypeMap.AUDIO.MPEG)]
    [TestCase("063.ttf", MimeTypeMap.APPLICATION.FONT_SFNT)]
    [TestCase("064.xltx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET, MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE)]
    [TestCase("065.dotx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT, MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE)]
    [TestCase("066.potx", MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION, MimeTypeMap.APPLICATION.VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE)]
    [TestCase("067.jfif", MimeTypeMap.IMAGE.JPEG)]
    [TestCase("068.cjs", MimeTypeMap.TEXT.PLAIN, MimeTypeMap.TEXT.JAVASCRIPT)]
    [TestCase("069.vcf", MimeTypeMap.TEXT.VCARD, MimeTypeMap.TEXT.X_VCARD)]
    [TestCase("070.wasm", MimeTypeMap.APPLICATION.WASM)]
    [TestCase("071.prql", MimeTypeMap.TEXT.PLAIN, MimeTypeMap.APPLICATION.PRQL)]
    [TestCase("072.xls", MimeTypeMap.APPLICATION.EXCEL)]
    public void GetMimeType_StreamsWithValidFileSignatures_ShouldReturnCorrectMimeTypes(string fileName, string expectedMimeTypeWithoutFileName, string expectedMimeTypeWithFileName = null)
    {
        using var fs = File.OpenRead(Path.Combine(SampleFilesDirectory, fileName));
        MimeTypeMap.GetMimeType(fs).ShouldBe(expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeType(fs, false).ShouldBe(expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeType(fs, true).ShouldBe(expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeTypes(fs).ShouldContain(expectedMimeTypeWithoutFileName);
        MimeTypeMap.TryGetMimeType(fs, out var matchingMimeType).ShouldBeTrue();
        matchingMimeType.ShouldBe(expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeType(fs, fileName).ShouldBe(expectedMimeTypeWithFileName ?? expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeType(fs, fileName, false).ShouldBe(expectedMimeTypeWithFileName ?? expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeType(fs, fileName, true).ShouldBe(expectedMimeTypeWithFileName ?? expectedMimeTypeWithoutFileName);
        MimeTypeMap.GetMimeTypes(fs, fileName).ShouldContain(expectedMimeTypeWithFileName ?? expectedMimeTypeWithoutFileName);
        MimeTypeMap.TryGetMimeType(fs, fileName, out matchingMimeType).ShouldBeTrue();
        matchingMimeType.ShouldBe(expectedMimeTypeWithFileName ?? expectedMimeTypeWithoutFileName);
    }

    [TestCase("800.css", MimeTypeMap.TEXT.CSS)]
    [TestCase("801.js", MimeTypeMap.APPLICATION.JAVASCRIPT)]
    [TestCase("802.html", MimeTypeMap.TEXT.HTML)]
    public void GetMimeType_TextStreamsWithFileNames_ShouldBeMatchedToSpecificMimeType(string fileName, string mimeType)
    {
        // Without a file name, these file signatures would have matched with "text/plain" MIME type.
        using var fs = File.OpenRead(Path.Combine(SampleFilesDirectory, fileName));
        MimeTypeMap.GetMimeType(fs, fileName).ShouldBe(mimeType);
        MimeTypeMap.GetMimeType(fs, fileName, false).ShouldBe(mimeType);
        MimeTypeMap.GetMimeType(fs, fileName, true).ShouldBe(mimeType);
        MimeTypeMap.GetMimeTypes(fs, fileName).ShouldContain(mimeType);
        MimeTypeMap.TryGetMimeType(fs, fileName, out var matchingMimeType).ShouldBeTrue();
        matchingMimeType.ShouldBe(mimeType);
    }
}
